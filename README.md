# OpenML dataset: JapaneseVowels

https://www.openml.org/d/375

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Mineichi Kudo, Jun Toyama, Masaru Shimbo    
**Source**: [UCI](https://archive.ics.uci.edu/ml/datasets/Japanese+Vowels)    
**Please cite**:   

**Japanese vowels**  
This dataset records 640 time series of 12 LPC cepstrum coefficients taken from nine male speakers.

The data was collected for examining our newly developed classifier for multidimensional curves (multidimensional time series). Nine male speakers uttered two Japanese vowels /ae/ successively. For each utterance, with the analysis parameters described below, we applied 12-degree linear prediction analysis to it to obtain a discrete-time series with 12 LPC cepstrum coefficients. This means that one utterance by a speaker forms a time series whose length is in the range 7-29 and each point of a time series is of 12 features (12 coefficients).

Similar data are available for different utterances /ei/, /iu/, /uo/, /oa/ in addition to /ae/. Please contact the donor if you are interested in using this data.

The number of the time series is 640 in total. We used one set of 270 time series for training and the other set of 370 time series for testing.

Analysis parameters:  
* Sampling rate : 10kHz
* Frame length : 25.6 ms
* Shift length : 6.4ms
* Degree of LPC coefficients : 12

Each line represents 12 LPC coefficients in the increasing order separated by spaces. This corresponds to one analysis
frame. Lines are organized into blocks, which are a set of 7-29 lines separated by blank lines and corresponds to a single speech utterance of /ae/ with 7-29 frames.

Each speaker is a set of consecutive blocks. In ae.train there are 30 blocks for each speaker. Blocks 1-30 represent speaker 1, blocks 31-60 represent speaker 2, and so on up to speaker 9. In ae.test, speakers 1 to 9 have the corresponding number of blocks: 31 35 88 44 29 24 40 50 29. Thus, blocks 1-31 represent speaker 1 (31 utterances of /ae/), blocks 32-66 represent speaker 2 (35 utterances of /ae/), and so on.

**Past Usage**

M. Kudo, J. Toyama and M. Shimbo. (1999). "Multidimensional Curve Classification Using Passing-Through Regions". Pattern Recognition Letters, Vol. 20, No. 11--13, pages 1103--1111.

If you publish any work using the dataset, please inform the donor. Use for commercial purposes requires donor permission.

References  

1. http://ips9.main.eng.hokudai.ac.jp/index_e.html
2. mailto:mine@main.eng.hokudai.ac.jp
3. mailto:jun@main.eng.hokudai.ac.jp
4. mailto:shimbo@main.eng.hokudai.ac.jp
5. http://kdd.ics.uci.edu/
6. http://www.ics.uci.edu/
7. http://www.uci.edu/

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/375) of an [OpenML dataset](https://www.openml.org/d/375). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/375/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/375/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/375/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

